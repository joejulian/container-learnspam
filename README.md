# container-learnspam

Learns spam and ham from [user]/Learn/Spam and [user]/Learn/Ham imap folders

Usage:

```
docker run -e SPAMSCAN_USER="my_imap_admin_user" -e SPAMSCAN_PASSWORD="change_me" -e SPAMSCAN_RSPAMD_CONTROLLER="rspamd" -ti --rm registry.gitlab.com/joejulian/container-learnspam:latest
```

FROM registry.gitlab.com/joejulian/container-rspamd:latest
USER root
RUN pacman -Syu --noconfirm python && pacman -Scc --noconfirm && rm -rf /var/cache/pacman/pkg
USER _rspamd
WORKDIR /spamscan
COPY spamscan.py .
CMD python spamscan.py
